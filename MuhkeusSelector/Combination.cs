﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MuhkeusSelector
{
    public class Combination
    {
        public int Hash1 { get; set; }
        public int Hash2 { get; set; }
        public int HashSum { get { return Hash1 | Hash2; } }
        public int HammingWeight { get { return CalculateHammingWeight(HashSum); } }

        public static int CalculateHammingWeight(int value)
        {
            value = value - ((value >> 1) & 0x55555555);
            value = (value & 0x33333333) + ((value >> 2) & 0x33333333);
            return (((value + (value >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
        }
    }
}
