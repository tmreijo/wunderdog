﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MuhkeusSelector
{
    public class CombinationCollector
    {
        public List<int> hashes { get; set; }
        private List<Combination> AllCombinations = new List<Combination>();
        private int maxHamming = 0;
        private Combination newCombination;

        public List<Combination> FindBestCombinations()
        {
            ConstructAllCombinations();
            return AllCombinations;
        }

        private void ConstructAllCombinations()
        {
            for (int i = 0; i < hashes.Count; i++)
                for (int j = i + 1; j < hashes.Count; j++)
                {
                    newCombination = new Combination()
                    {
                        Hash1 = hashes[i],
                        Hash2 = hashes[j]
                    };

                    if (NewCombinationIsBetterThanPreviousOnes())
                    {
                        StartNewCollection();
                    }
                    else if (NewCombinationIsSameLevel())
                        AddToExistingCollection();
                }
        }

        private void AddToExistingCollection()
        {
            AllCombinations.Add(newCombination);
        }

        private bool NewCombinationIsSameLevel()
        {
            return newCombination.HammingWeight == maxHamming;
        }

        private void StartNewCollection()
        {
            AllCombinations.Clear();
            AllCombinations.Add(newCombination);
            maxHamming = newCombination.HammingWeight;
        }

        private bool NewCombinationIsBetterThanPreviousOnes()
        {
            return newCombination.HammingWeight > maxHamming;
        }
    }
}
