﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace MuhkeusSelector
{
    public class FileReader
    {
        private static string regexpSplitWithoutCharactersInWord = "[^{0}]";
        private char[] acceptableCharacters;
        private string regexp;
        private WordStorage wordVault;
        string currentLine;

        public string AcceptableCharacters
        {
            set
            {
                acceptableCharacters = value.Replace(" ", "").Replace(",", "").ToCharArray();
                wordVault = new WordStorage(acceptableCharacters);
                regexp = String.Format(regexpSplitWithoutCharactersInWord, new String(acceptableCharacters));
            }
        }

        public string FileName { get; set; }

        public Dictionary<int, HashSet<string>> CollectIndividualWordsFromFile()
        {
            using (StreamReader sr = File.OpenText(FileName))
            {
                while ((currentLine = sr.ReadLine()) != null)
                {
                    CollectIndividualWordsFromLine();
                }
            }
            return wordVault.IndividualWords;
        }

        private void CollectIndividualWordsFromLine()
        {
            foreach (string word in SplitLineToWords())
            {
                wordVault.TryAddWord(word);
            }
        }

        private string[] SplitLineToWords()
        {
            return Regex.Split(currentLine, @regexp, RegexOptions.IgnoreCase);
        }
    }
}
