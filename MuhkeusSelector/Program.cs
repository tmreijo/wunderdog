﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MuhkeusSelector
{
    class Program
    {
        private static string characters = "a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, å, ä, ö";

        static void Main(string[] args)
        {
            Dictionary<int, HashSet<string>> individualWords = ReadIndividualWordsFromFile();
            List<Combination> bestCombinations = FindBestCombinations(individualWords);
            PrintCombinations(individualWords, bestCombinations);
        }

        private static void PrintCombinations(Dictionary<int, HashSet<string>> individualWords, List<Combination> bestCombinations)
        {
            WordComboPrinter printer = new WordComboPrinter()
            {
                Combinations = bestCombinations,
                words = individualWords
            };

            printer.PrintCombinations();
        }

        private static List<Combination> FindBestCombinations(Dictionary<int, HashSet<string>> individualWords)
        {
            CombinationCollector combonator = new CombinationCollector()
            {
                hashes = individualWords.Keys.ToList()
            };
            List<Combination> bestCombinations = combonator.FindBestCombinations();
            return bestCombinations;
        }

        private static Dictionary<int, HashSet<string>> ReadIndividualWordsFromFile()
        {
            FileReader reader = new FileReader()
            {
                AcceptableCharacters = characters,
                FileName = "alastalon_salissa.txt"
            };
            Dictionary<int, HashSet<string>> individualWords = reader.CollectIndividualWordsFromFile();
            return individualWords;
        }
    }
}
