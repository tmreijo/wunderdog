﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MuhkeusSelector
{
    public class WordComboPrinter
    {
        public List<Combination> Combinations { get; set; }
        public Dictionary<int, HashSet<string>> words { get; set; }

        public void PrintCombinations()
        {
            foreach (Combination combo in Combinations)
            {
                Console.Write("Muhkeus " + combo.HammingWeight);
                PrintWords(combo.Hash1);
                PrintWords(combo.Hash2);
                Console.WriteLine();
            }
        }

        public void PrintWords(int hash)
        {
            HashSet<string> wordset;
            words.TryGetValue(hash, out wordset);
            PrintWords(wordset);
        }

        public void PrintWords(HashSet<string> wordset)
        {
            foreach (string word in wordset)
                Console.Write(" " + word);
        }
    }
}
