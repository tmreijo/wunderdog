﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MuhkeusSelector
{
    public class WordHashGenerator
    {
        private Dictionary<char, int> charTable;

        public WordHashGenerator(char[] chars)
        {
            charTable = ConvertCharactersToMaskDictionary(chars);
        }

        private Dictionary<Char,int> ConvertCharactersToMaskDictionary(char[] chars)
        {
            Dictionary<char, int> result = new Dictionary<char, int>();
            for (int i = 0; i < chars.Length; i++)
                result.Add(chars[i], 1 << i);
            return result;
        }

        public int CalculateWordHash(string word)
        {
            int hash = 0;
            foreach (char character in word)
            {
                int charMask;
                charTable.TryGetValue(Char.ToLower(character), out charMask);
                hash = hash | charMask;
            }
            return hash;
        }
    }
}
