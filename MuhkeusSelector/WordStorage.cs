﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MuhkeusSelector
{
    public class WordStorage
    {
        public Dictionary<int, HashSet<string>> IndividualWords { get; set; }
        private WordHashGenerator hashGenerator;

        private string word;
        private int newWordHash;

        public WordStorage(char[] characters)
        {
            hashGenerator = new WordHashGenerator(characters);
            IndividualWords = new Dictionary<int, HashSet<string>>();
        }

        public void TryAddWord(string newWord)
        {
            if (newWord != string.Empty)
            {
                Initialize(newWord);
                CompareWordAgainsExistingWords();
            }
        }

        private void Initialize(string newWord)
        {
            word = newWord;
            newWordHash = GenerateWordHash();
        }

        private int GenerateWordHash()
        {
            return hashGenerator.CalculateWordHash(word);
        }

        private void CompareWordAgainsExistingWords()
        {
            if (HashExists())
                AddWordToExistingHash();
            else
                AddWordAsNew();
        }

        private bool HashExists()
        {
            return IndividualWords.ContainsKey(newWordHash);
        }

        private void AddWordToExistingHash()
        {
            IndividualWords[newWordHash].Add(word);
        }

        private void AddWordAsNew()
        {
            IndividualWords.Add(newWordHash, new HashSet<String>() { word });
        } 
    }
}
